# boot.py -- run on boot-up
# can run arbitrary Python, but best to keep it minimal

import pyb
from scripts import dac_loop
#pyb.main('main.py') # main script to run after this one
pyb.usb_mode('CDC') # act as a serial and a storage device
#pyb.usb_mode('CDC+HID') # act as a serial device and a mouse

import scripts.dac_loop
dac_loop.loop_now()
