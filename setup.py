#!/usr/bin/python
# -*- coding: utf-8 -*-


__author__ = "Carlos Mão de Ferro"
__copyright__ = ""
__credits__ = "Carlos Mão de Ferro"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"
__updated__ = "2016-04-27"

from setuptools import setup, find_packages
import re

version = ''
with open('upyuploader/__init__.py', 'r') as fd:
    version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
                        fd.read(), re.MULTILINE).group(1)

if not version:
    raise RuntimeError('Cannot find version information')


setup(

    name='uPyUploader',
    version=version,
    description="""upload code to micropython boardF""",
    author='Carlos Mao de Ferro',
    author_email='carlos.maodeferro@neuro.fchampalimaud.org',
    license='MIT',
    packages=find_packages(exclude=['contrib', 'examples', 'docs', 'tests']),
    install_requires=[
        "pyserial >= 2.7",
        "Send2Trash >= 1.3.0"
    ],
)
