# !/usr/bin/python3
# -*- coding: utf-8 -*-
""" pyControlGUI

"""
# create logger

import logging
import os
from board.exceptions.pyboard_error import PyBoardError
from board.exceptions.pyboard_error_exec import PyBoardErrorExec
from board.pycboard import Pycboard
from upyuploader import settings


logger = logging.getLogger("upyuploader")
logger.setLevel(logging.DEBUG)

# create file handler which logs even debug messages
fh = logging.FileHandler('{0}.log'.format(settings.APP_LOG_FILENAME))
fh.setLevel(settings.APP_LOG_HANDLER_FILE_LEVEL)

# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(settings.APP_LOG_HANDLER_CONSOLE_LEVEL)

# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(name)s | %(module)s | %(funcName)s | %(message)s', datefmt='%d/%m/%Y %I:%M:%S')
fh.setFormatter(formatter)
ch.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)

logger.debug("log upyuploader is now set up")
logger.info("Started uPyUploader")

myboard = Pycboard(settings.serial_port)
uid = myboard.get_unique_ID()
logger.info("Board unique id: %s", uid)

try:
    if settings.upload_boot:
        logger.info("Uploading boot file: %s", settings.boot_file_path)
        myboard.transfer_file(settings.boot_file_path)
        logger.info("Success!")
except PyBoardError as err:
    logger.error(str(err))

# try:
#     logger.info("Uploading folder: %s", settings.upload_folder_path)
#     myboard.transfer_folder(settings.upload_folder_path, override=True)
#     logger.info("Success!")
# except PyBoardErrorExec as err:
#     logger.error(str(err))


for script_name in settings.scripts:
    try:
        script_path = os.path.join(settings.upload_folder_path, script_name)
        logger.info("Uploading script: %s", script_path)
        try:
            myboard.remove_file(script_name)
        except Exception as err:
            logger.error(str(err))
        myboard.transfer_file(script_path)
        logger.info("Success!")
    except PyBoardErrorExec as err:
        logger.error(str(err))

myboard.exit_raw_repl()
myboard.close()

logger.info("Connection closed")
