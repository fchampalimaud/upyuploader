import logging

# logging settings
APP_LOG_FILENAME = 'upyuploader'
APP_LOG_HANDLER_FILE_LEVEL = logging.DEBUG
APP_LOG_HANDLER_CONSOLE_LEVEL = logging.DEBUG

# board settings
upload_boot = False
serial_port = '/dev/tty.usbmodem1462'
upload_folder_path = '/Users/carlos/Programador/micropython-uploader/scripts'
boot_file_path = '/Users/carlos/Programador/micropython-uploader/util/boot.py'
#scripts = ['timer_dac.py', 'timer_dac2.py']
scripts = ['accel.py']