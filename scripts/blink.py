from pyb import LED
from pyb import delay

led = LED(1)  # red led

def start_blinking():

    print('Start blinking!')
    
    for i in range(10):
        led.toggle()
        delay(500)
    
    print('Done!')