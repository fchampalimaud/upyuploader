#

import pyb
from pyb import Pin
from pyb import ADC
from pyb import delay
from pyb import ExtInt
from pyb import DAC
from pyb import Timer

my_control = True


def square_wave_output(e):
    global my_control
    if my_control:
        dac.write(255)
        my_control = False
    else:
        dac.write(0)
        my_control = True

dac = DAC(Pin('X5'))

tim = Timer(4)              # create a timer object using timer 8
tim.init(freq=100)                # trigger at 2Hz
tim.callback(square_wave_output)
