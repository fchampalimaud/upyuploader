from pyb import Pin
from pyb import DAC
from pyb import delay

def loop_now(timeout=5):
    dac = DAC(Pin('X5'))
    my_control = True
    print("looping now...")
    while(True):
        if my_control:
            dac.write(0)
            my_control = False
        else:
            dac.write(255)
            my_control = True
        delay(timeout)
