import pyb
from pyb import Pin
from pyb import ADC
from pyb import delay
from pyb import ExtInt
from pyb import DAC

my_control = True
update = False

def square_wave_output(e):
    global update
    update = True

dac = DAC(Pin('X5'))

ext = ExtInt(Pin('Y11'), ExtInt.IRQ_RISING_FALLING, Pin.PULL_NONE, square_wave_output)  # BNC1

while(True):
    if update:
        update = False
        if my_control:
            dac.write(255)
            my_control = False
        else:
            dac.write(0)
            my_control = True