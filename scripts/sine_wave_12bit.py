# sine_wave_12bit.py
# output a continuous sine-wave at 12-bit resolution

import math
from array import array
from pyb import DAC
from pyb import Pin

# create a buffer containing a sine-wave, using half-word samples
buf = array('H', 2048 + int(2047 * math.sin(2 * math.pi * i / 128)) for i in range(128))

# output the sine-wave at 400Hz
dac = DAC(Pin('X5'), bits=12)
dac.write_timed(buf, 400 * len(buf), mode=DAC.CIRCULAR)
