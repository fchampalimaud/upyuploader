from pyb import delay
from pyb import Pin

my_pin = Pin('X18', Pin.OUT_PP)

def loop_now():
    while(True):
        if not my_pin.value():
            my_pin.high()
        else:
            my_pin.low()
        delay(500)
