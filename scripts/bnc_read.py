from pyb import Pin
from pyb import ADC
from pyb import delay


def run(timeout=100):
    adc = ADC(Pin('Y11'))  # BNC1
    while(True):
        val = adc.read()  # read value, 0-4095
        print(val)
        delay(timeout)
