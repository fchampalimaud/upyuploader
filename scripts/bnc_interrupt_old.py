import pyb
from pyb import Pin
from pyb import ADC
from pyb import delay
from pyb import ExtInt
from pyb import DAC

#
#ext_rise = ExtInt(Pin('Y11'), ExtInt.IRQ_RISING_FALLING, Pin.PULL_NONE, lambda e: print(e))


class BNC_Interrupt(object):
    # prev_timestamp = 0

    def __init__(self):
        # self.prev_timestamp = pyb.millis()
        self.dac = DAC(Pin('X5'))
        # self.adc = ADC(Pin('Y11'))  # BNC1
        self.my_control = True

    def start(self):
        # self.ext = ExtInt(Pin('Y11'), ExtInt.IRQ_FALLING, Pin.PULL_NONE, lambda e: self.print_diff())  # BNC1
        self.ext = ExtInt(Pin('Y11'), ExtInt.IRQ_RISING_FALLING, Pin.PULL_NONE, lambda e: self.square_wave_output())  # BNC1

    def print_diff(self):
        #diff = pyb.millis() - self.prev_timestamp
        # print(1000//diff)
        # print(diff)
        #self.prev_timestamp = pyb.millis()
        # print(self.adc.read())  # read value, 0-4095
        pass

    def square_wave_output(self):
        if self.my_control:
            self.dac.write(255)
            self.my_control = False
        else:
            self.dac.write(0)
            self.my_control = True


bnc = BNC_Interrupt()
bnc.start()
