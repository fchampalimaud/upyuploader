# We start by creating a tuple of LED objects for the x and y directions.
# Tuples are immutable objects in python which means they can't be modified once they are created.
# We then proceed as before but turn on a different LED for positive and negative x values.
# We then do the same for the y direction. This isn't particularly sophisticated but it does the job.
# Run this on your pyboard and you should see different LEDs turning on depending on how you tilt the board.
# https://micropython.org/doc/tut-accel

import pyb

xlights = (pyb.LED(2), pyb.LED(3))
ylights = (pyb.LED(1), pyb.LED(4))

accel = pyb.Accel()
SENSITIVITY = 3

while True:
    x = accel.x()
    if x > SENSITIVITY:
        xlights[0].on()
        xlights[1].off()
    elif x < -SENSITIVITY:
        xlights[1].on()
        xlights[0].off()
    else:
        xlights[0].off()
        xlights[1].off()

    y = accel.y()
    if y > SENSITIVITY:
        ylights[0].on()
        ylights[1].off()
    elif y < -SENSITIVITY:
        ylights[1].on()
        ylights[0].off()
    else:
        ylights[0].off()
        ylights[1].off()

    pyb.delay(100)
