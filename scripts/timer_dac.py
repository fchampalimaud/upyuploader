import pyb
from pyb import Pin
from pyb import ADC
from pyb import delay
from pyb import ExtInt
from pyb import DAC
from pyb import Timer

my_control = True
update = False


def square_wave_output(e):
    global update
    update = True

dac = DAC(Pin('X5'))

tim = Timer(4) # create a timer object using timer 4
tim.init(freq=100) # trigger at 100Hz
tim.callback(square_wave_output)

while(True):
    if update:
        update = False
        if my_control:
            dac.write(255)
            my_control = False
        else:
            dac.write(0)
            my_control = True