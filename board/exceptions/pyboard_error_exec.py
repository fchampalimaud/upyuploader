# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrolapi.exceptions.pyboard_error

"""


__author__ = "Carlos Mão de Ferro and Ricardo Ribeiro"
__copyright__ = ""
__cred_its__ = "Carlos Mão de Ferro and Ricardo Ribeiro"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"
__updated__ = "2016-04-12"


class PyBoardErrorExec(Exception):

    def __init__(self, command, board_exception):
        self.command = command
        self.board_exception = board_exception

    def __str__(self):
        return "Could not execute pyboard command '{0}':\n{1}".format(self.command, self.board_exception.decode("utf-8"))
