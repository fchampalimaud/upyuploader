import os
import logging

from board.pyboard import Pyboard
from board.exceptions.pyboard_error import PyBoardError
from board.exceptions.pyboard_no_framework_error import PyBoardNoFrameworkError
from board.exceptions.pyboard_error_exec import PyBoardErrorExec

__author__ = "Thomas Akam"
__copyright__ = ""
__credits__ = "Thomas Akam"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"

# ----------------------------------------------------------------------------------------
#  Pycboard class.
# ----------------------------------------------------------------------------------------


class Pycboard(Pyboard):
    FRAMEWORK_NAME = 'pyControl'
    """
    Pycontrol board inherits from Pyboard and adds functionallity for file transfer
    and pyControl operations.
    """

    def __init__(self, serial_device, baudrate=115200):
        self.logger = logging.getLogger(__name__)
        super().__init__(serial_device, baudrate=115200)

        self.reset()

    def reset(self):
        'Enter raw repl (soft reboots pyboard) but do not import modules'
        self.enter_raw_repl()
        self.exec('import os')
        self.framework_running = False
        self.data = None

    def get_unique_ID(self):
        unique_id = eval(self.eval('pyb.unique_id()').decode())
        self.logger.debug("%s", "Board unique id: {0}".format(unique_id))

        return unique_id

    def import_pycontrol(self):
        """ Raises error if cannot import
            Note that an empty pyControl folder will not raise error 
            because python allows to import empty folders
        """
        try:
            self.exec('from pyControl import *')
        except PyBoardErrorExec as err:
            self.logger.error(str(err))
            raise PyBoardError('Cannot import pyControl. Is framework installed?', err)

    # ------------------------------------------------------------------------------------
    # File transfer
    # ------------------------------------------------------------------------------------

    def write_file(self, target_path, data):
        '''Write data to file at specified path on pyboard, any data already
        in the file will be deleted.
        '''
        self.exec("tmpfile = open('{}','w')".format(target_path))
        self.exec("tmpfile.write({data})".format(data=repr(data)))
        self.exec('tmpfile.close()')

    def transfer_file(self, file_path, target_path=None):
        '''Copy a file into the root directory of the pyboard.'''
        if not target_path:
            target_path = os.path.split(file_path)[-1]
        transfer_file = open(file_path)
        self.write_file(target_path, transfer_file.read())
        transfer_file.close()
        # logger.debug("Transfered file {}".format(file_path))

    def transfer_folder(self, folder_path, target_folder=None, file_type='all', override=False):
        '''Copy a folder into the root directory of the pyboard.  Folders that
        contain subfolders will not be copied successfully.  To copy only files of
        a specific type, change the file_type argument to the file suffix (e.g. 'py').'''
        if not target_folder:
            target_folder = os.path.basename(folder_path)
        files = os.listdir(folder_path)
        self.logger.debug("Target folder name: %s", target_folder)
        self.logger.debug("Target folder content: %s", files)
        if file_type != 'all':
            files = [f for f in files if f.split('.')[-1] == file_type]

        if override:
            try:
                target_folder_exists = False
                self.exec('os.listdir({0})'.format(repr(target_folder)))
                target_folder_exists = True
            except PyBoardErrorExec as err:
                self.logger.debug("Target folder doesn't exist: %s", str(err), exc_info=True)

            if target_folder_exists:
                self.exec('for name in os.listdir({0}): os.remove({0} + {1} + name)'.format(repr(target_folder), repr('/')))
                self.exec('os.rmdir({})'.format(repr(target_folder)))
                self.logger.debug("Removed old folder: %s", target_folder)

        try:
            self.exec('os.mkdir({})'.format(repr(target_folder)))
            self.logger.debug("Created new folder: %s", target_folder)
        except PyBoardError as err:
            self.logger.debug(str(err))

        for f in files:
            file_path = os.path.join(folder_path, f)
            target_path = target_folder + '/' + f
            self.transfer_file(file_path, target_path)

    def remove_folder(self, folder_path):
        if folder_path and os.path.exists(folder_path):
            self.exec('for name in os.listdir({0}): os.remove({0} + {1} + name)'.format(repr(folder_path), repr('/')))
            self.exec('os.rmdir({})'.format(repr(folder_path)))
            self.logger.debug("Removed folder: %s", folder_path)

    def remove_file(self, file_path):
        'Remove a file from the pyboard.'
        self.exec('os.remove({})'.format(repr(file_path)))

    # ------------------------------------------------------------------------------------
    # pyControl operations.
    # ------------------------------------------------------------------------------------

    def install_framework(self, framework_path, override=False):
        if framework_path:
            try:
                if self.FRAMEWORK_NAME not in os.listdir(framework_path):
                    raise PyBoardNoFrameworkError("Framework path does not contain folder '{0}'!".format(self.FRAMEWORK_NAME))
                self.logger.debug('Transfering pyControl framework to pyboard...')
                framework_full_path = os.path.join(framework_path, self.FRAMEWORK_NAME)
                self.transfer_folder(framework_full_path, file_type='py', override=override)
                self.logger.debug('Success!')
            except PyBoardError as err:
                self.logger.error("Framework not installed or invalid path provided.", exc_info=True)
                raise PyBoardNoFrameworkError("Problems while installing framework: {0}".format(str(err)), err)
        else:
            self.logger.error("Framework path not specified")
            raise PyBoardNoFrameworkError("Framework path not specified")

    def load_framework(self):
        """ Try to load pyControl framework"""
        self.logger.debug("Loading pyControl framework...")
        self.import_pycontrol()
        self.logger.debug("Framework successfully loaded!")

    def load_state_machine(self, sm_name):
        self.logger.debug("Loading state machine %s...", sm_name)
        try:
            self.exec('import {}'.format(sm_name))
            self.remove_file(sm_name + Labels.TASK_FILE_EXTENSION)
        except PyBoardErrorExec as err:
            self.logger.error(str(err))
            raise PyBoardError("Task exists but cannot import!", err)

        try:
            self.exec(sm_name + '_instance = sm.State_machine({})'.format(sm_name))
        except PyBoardErrorExec as err:
            self.logger.error(str(err))
            raise PyBoardError("Task exists but cannot load!", err)
        self.logger.debug("State machine successfully loaded!")

    def upload_state_machine(self, sm_name='', sm_path=None):
        """ 
        Instantiate state machine object as sm_name_instance. 
        """

        self.logger.debug("Uploading state machine: %s", sm_name)

        if not os.path.exists(sm_path):
            raise PyBoardError('State machine file not found at: ' + sm_path)

        try:
            self.transfer_file(sm_path)
        except PyBoardErrorExec as err:
            self.logger.error(str(err))
            raise PyBoardError("Cannot transfer task!", err)

    def print_IDs(self):
        'Print state and event IDs.'
        try:
            ID_info = self.exec('fw.print_IDs()').decode()
        except PyBoardErrorExec as err:
            self.logger.error(str(err))
            raise PyBoardError("Cannot print task IDs! Is task uploaded?", err)
        return ID_info

    def print_states(self):
        'Print states as a dictionary'
        self.logger.debug("Printing states...")
        try:
            states = self.exec('fw.print_states()').decode()
        except PyBoardErrorExec as err:
            self.logger.error(str(err))
            raise PyBoardError("Cannot print task states! Is task uploaded?", err)
        if not states:
            self.logger.error("States string is empty! Is task uploaded?")
            raise PyBoardError("States string is empty! Is task uploaded?")
        self.logger.debug("States: %s", states)
        return states

    def print_events(self):
        'Print events as a dictionary'
        self.logger.debug("Printing events...")
        try:
            events = self.exec('fw.print_events()').decode()
        except PyBoardErrorExec as err:
            self.logger.error(str(err))
            raise PyBoardError("Cannot print task events! Is task uploaded?", err)
        if not events:
            self.logger.error("Events string is empty! Is task uploaded?")
            raise PyBoardError("Events string is empty! Is task uploaded?")
        self.logger.debug("Events: %s", events)
        return events

    def start_framework(self, dur=None, verbose=False, data_output=True):
        'Start pyControl framwork running on pyboard.'
        self.exec('fw.verbose = ' + repr(verbose))
        self.exec('fw.data_output = ' + repr(data_output))
        self.exec_raw_no_follow('fw.run({})'.format(dur))
        self.framework_running = True
        self.data = b''

    def stop_framework(self):
        'Stop framework running on pyboard by sending stop command.'
        self.serial.write(b'E')
        self.framework_running = False
        data_err = self.read_until(2, b'\x04>', timeout=10)

    def process_data(self):
        'Process data output from the pyboard to the serial line.'
        data_string = None
        data_err = None
        while self.serial.inWaiting() > 0:
            self.data = self.data + self.serial.read(1)
            if self.data.endswith(b'\x04'):  # End of framework run.
                self.framework_running = False
                data_err = self.read_until(2, b'\x04>', timeout=10)
                break
            elif self.data.endswith(b'\n'):  # End of data line.
                data_string = self.data.decode()
                self.data = b''

        return (data_err, data_string)

    def run_framework(self, dur=None, verbose=False):
        '''Run framework for specified duration (seconds).'''
        self.start_framework(dur, verbose)
        try:
            while self.framework_running:
                self.process_data()
        except KeyboardInterrupt:
            self.stop_framework()

    # ------------------------------------------------------------------------------------
    # Getting and setting variables.
    # ------------------------------------------------------------------------------------

    def set_variable(self, sm_name, v_name, v_value):
        'Set state machine variable when framework not running.'
        if self._check_variable_exits(sm_name, v_name):
            set_value = None
            v_value = eval(v_value)
            while set_value != v_value:
                try:
                    self.exec(sm_name + '_instance.sm.v.' + v_name + '=' + repr(v_value))
                    set_value = self.get_variable(sm_name, v_name)
                except PyBoardError as e:
                    self.logger.warning(e)
            return set_value

    def get_variable(self, sm_name, v_name):
        'Get value of state machine variable when framework not running.'
        if self._check_variable_exits(sm_name, v_name):
            v_value = None
            while v_value == None:
                try:
                    self.serial.flushInput()
                    v_value = self.eval(sm_name + '_instance.sm.v.' + v_name).decode()
                    return eval(v_value)
                except Exception as err:
                    self.logger.error("Cannot get variable %s: %s", v_value, str(err), exc_info=True)
                    raise PyBoardError(str(err), err)

    def _check_variable_exits(self, sm_name, v_name):
        try:
            self.exec(sm_name + '_instance')
        except PyBoardErrorExec as err:
            self.logger.warning("State machine not found. Did you upload first?", exc_info=True)
            raise PyBoardError("State machine not found. Did you upload first?", err)
        try:
            self.exec(sm_name + '_instance.sm.v.' + v_name)
        except PyBoardErrorExec as e:
            self.logger.debug('Variable not set: invalid variable name.\n')
            self.logger.debug('Pyboard error message: \n\n' + str(e))
            return False
        return True

if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    logger = logging.getLogger()

    pycboard = Pycboard('/dev/tty.usbmodem1472', framework_path='/Users/carlos/Programador/pyControlGUI/examples/1_board_per_setup/frameworks/breakout_1_0')

    pycboard.close()
